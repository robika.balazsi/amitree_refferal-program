// TODO: count the steps and detect if the game ended without winner (table is full) Remarks: I mentioned this scenario to Marcus.
const ROW_NUMBER = 6
const COLUMN_NUMBER = 7
const RED_PLAYER = 1
const BLUE_PLAYER = -1

const statusTable = [] // contains the current status of the game
let currentPlayer = RED_PLAYER // flag that shows who is the actual player
let table

document.addEventListener("DOMContentLoaded", init)

// adding DOM items to the table and initialize status table
function init() {
  table = document.getElementById('table')

  for (let i = 0; i < ROW_NUMBER; i++) {
    table.appendChild(document.createElement('tr'))
    statusTable[i] = []

    for (let j = 0; j < COLUMN_NUMBER; j++) {
      table.rows[i].appendChild(document.createElement('td'))
      table.rows[i].cells[j].onclick = () => clickToCell(j)
      statusTable[i][j] = null
    }
  }
}

// handle click event
function clickToCell(column) {
  try {
    let row = insert(currentPlayer, column)
    if (isWinner(currentPlayer, row, column)) {
      console.log('The winner is:', currentPlayer)
      return
    }
    currentPlayer = -1 * currentPlayer
  } catch (err) {
    console.log(err)
  }
}

// if the insert operations has closed sucessfully the function returns the row index
function insert(player, column) {
  let index = ROW_NUMBER - 1
  while (index >= 0 && statusTable[index][column]) {
    index = index - 1
  }

  if (index < 0) {
    console.log('This column is already full')
    throw new Error('Incorrect step!')
  }

  statusTable[index][column] = player
  table.rows[index].cells[column].innerHTML = `<span class="ball" style="background-color: ${player == RED_PLAYER ? "red" : "blue"}"></span>`

  return index
}

// this function decides that the last step was winner step or not
function isWinner(player, row, column) {
  // horizontal -> v:0 h:1/-1
  if (scanNeighborhood(player, row, column, 0, -1) + scanNeighborhood(player, row, column, 0, +1) - 1 >= 4) {
    return true
  }

  // vertical -> v: 1/-1 h: 0
  if (scanNeighborhood(player, row, column, -1, 0) + scanNeighborhood(player, row, column, +1, 0) - 1 >= 4) {
    return true
  }

  // diagonal -> v: 1/-1 h: -1/1
  if (scanNeighborhood(player, row, column, 1, -1) + scanNeighborhood(player, row, column, -1, 1) - 1 >= 4) {
    return true
  }

  // diagonal -> v: 1/-1 h: 1/-1
  if (scanNeighborhood(player, row, column, 1, 1) + scanNeighborhood(player, row, column, -1, -1) - 1 >= 4) {
    return true
  }

  return false
}


function scanNeighborhood(player, row, column, verticalDirection, horizontalDirection) {
  if (row >= ROW_NUMBER || row < 0) {
    return 0
  }

  if (column >= COLUMN_NUMBER || column < 0) {
    return 0
  }

  if (statusTable[row][column] == player) {
    return 1 + scanNeighborhood(player, row + verticalDirection, column + horizontalDirection, verticalDirection, horizontalDirection)
  }

  return 0
}
