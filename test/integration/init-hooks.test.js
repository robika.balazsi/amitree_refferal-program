const mongoose = require('mongoose')

let potentialDataLoss = false

before(() => {
  if (process.env.NODE_ENV != 'test') {
    potentialDataLoss = true
    throw new Error(`Trying to execut integration test in ${process.env.NODE_ENV} environment! Be carefull because the tests can damage your data!`)
  }
})

after(async () => {
  if (!potentialDataLoss) {
    await mongoose.connection.db.dropDatabase()
  }
})
