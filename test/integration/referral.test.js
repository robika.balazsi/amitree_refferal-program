const { expect } = require('chai')
const request = require('supertest')
const httpStatus = require('http-status')
const jsonWebToken = require('jsonwebtoken')
const config = require('../../src/config/config')
const User = require('../../src/model/user')
const api = require('../../src/api')

describe('Referral logic related API calls', () => {
  let user
  let token

  before(async () => {
    user = await new User({
      email: 'test@test.com',
      name: 'Robert-Sandor Balazsi',
      password: 'testtest'
    }).save()
    token = jsonWebToken.sign({ id: user._id, role: 'User' }, config.secret)
  })

  after(async () => {
    await User.deleteMany()
  })

  it('should provides the referral link', (done) => {
    request(api)
      .get('/api/referrals')
      .set('Authorization', `Bearer ${token}`)
      .expect(httpStatus.OK)
      .then(res => {
        expect(res.body.link).to.be.a('string')
        expect(res.body.link.split('/r/')[1].length).to.be.lte(18)
        done()
      })
      .catch(done)
  })
})