/* eslint-disable no-undef */
const { expect } = require('chai')
const request = require('supertest')
const httpStatus = require('http-status')
const jsonWebToken = require('jsonwebtoken')
const passwordHash = require('password-hash')
const config = require('../../src/config/config')
const User = require('../../src/model/user')
const api = require('../../src/api')

describe('# User related API calls', () => {

  describe('GET /api/users/all', () => {
    let token

    before(() => {
      token = jsonWebToken.sign({ email: 'test@test.com', role: 'Admin' }, config.secret)
    })

    it('should return a list with users', (done) => {
      request(api)
        .get('/api/users/all')
        .set('Authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body).to.be.an('array')
          done()
        })
        .catch(done)
    })
  })

  describe('POST /api/users', () => {
    let existingUser
    let newUser

    before(async () => {
      existingUser = {
        name: 'existingUser',
        email: 'user@exists.com',
        password: '123546asfsadf'
      }
      newUser = {
        name: 'newUser',
        email: 'user@new.com',
        password: '123546asfsadf'
      }
      await User.deleteMany()
      await new User(existingUser).save()
    })

    after(async () => {
      await User.deleteMany()
    })

    it('should reject the request because invalid emai parameter', (done) => {
      request(api)
        .post('/api/users')
        .send({
          email: 'incorrect@.com',
          name: 'testName',
          password: '456321asfasdf'
        })
        .expect(httpStatus.BAD_REQUEST)
        .then(res => {
          expect(res.body.message).to.have.string('mail')
          done()
        })
        .catch(done)
    })

    it('should reject the request because invalid password parameter', (done) => {
      request(api)
        .post('/api/users')
        .send({
          email: 'correct@email.com',
          name: 'testName',
          password: 'noNumber'
        })
        .expect(httpStatus.BAD_REQUEST)
        .then(res => {
          expect(res.body.message).to.have.string('password')
          done()
        })
        .catch(done)
    })

    it('should reject the request because the user already exist', (done) => {
      request(api)
        .post('/api/users')
        .send(existingUser)
        .expect(httpStatus.BAD_REQUEST)
        .then(res => {
          expect(res.body.message).to.have.string('already')
          expect(res.body.message).to.have.string('exists')
          done()
        })
        .catch(done)
    })

    it('should approve the sign up request', (done) => {
      request(api)
        .post('/api/users')
        .send(newUser)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body).to.have.own.property('id')
          expect(res.body).to.have.own.property('email')
          expect(res.body).to.have.own.property('name')
          done()
        })
        .catch(done)
    })
  })

  describe('POST /api/users/authenticate', () => {
    let user

    before(async () => {
      user = await new User({
        email: 'test@test.com',
        name: 'test-user',
        password: passwordHash.generate('testtest')
      }).save()
    })

    after(async () => {
      await User.deleteMany()
    })

    it('should reject the sign in because the credentials are incorrect', (done) => {
      request(api)
        .post('/api/users/authenticate')
        .send({
          email: user.email,
          password: 'testtestWRONG'
        })
        .expect(httpStatus.BAD_REQUEST, done)
    })

    it('should accept the request and sign in the user', (done) => {
      request(api)
        .post('/api/users/authenticate')
        .send({
          email: user.email,
          password: 'testtest'
        })
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body).to.have.own.property('user')
          expect(res.body).to.have.own.property('token')
          done()
        })
        .catch(done)
    })
  })
})
