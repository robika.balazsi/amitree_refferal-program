/* eslint-disable no-undef */
const { expect } = require('chai')
const sinon = require('sinon')

const jsonWebToken = require('jsonwebtoken')
const passwordHash = require('password-hash')
const userController = require('../../src/controller/user')
const User = require('../../src/model/user')
const DTOMappers = require('../../src/model/data-tranfer-object/mappers')
const logger = require('../../src/helper/logger')
const validationResultModule = require('../../src/helper/custom-express-validator')


describe('# User related logic unit test', () => {
  let req
  let res
  let next
  let sandbox
  let mapperStub
  let findOneStub
  let validationResultStub

  before(() => {
    req = {
      body: {},
      get: () => null
    }
    res = {
      send: sinon.spy()
    }
    next = sinon.spy()
    findOneStub = sinon.stub(User, 'findOne')
    mapperStub = sinon.stub(DTOMappers, 'mapUserDTO')
    validationResultStub = sinon.stub(validationResultModule, 'validationResult')

    sandbox = sinon.createSandbox()
    sandbox.stub(logger, 'error')
    sandbox.stub(logger, 'info')
  })

  afterEach(() => {
    res.send.resetHistory()
    next.resetHistory()
    mapperStub.reset()
  })

  describe('# getUsers()', () => {
    let findStub

    before(() => {
      findStub = sinon.stub(User, 'find')
    })
    afterEach(() => {
      findStub.reset()
    })

    it('should call the User model\'s \'find\' method', async () => {
      findStub.resolves([])
      await userController.getUsers(req, res, next)
      expect(findStub.calledOnce).to.be.true
      expect(res.send.calledOnce).to.be.true
    })

    it('should map all the user objects to DTO objects before the return', async () => {
      findStub.resolves([{}, {}])
      mapperStub.returns({})
      await userController.getUsers(req, res, next)
      expect(mapperStub.calledTwice).to.be.true
      expect(findStub.calledOnce).to.be.true
      expect(res.send.calledOnce).to.be.true
    })

    it('should report the error via the next funtion if something goes wrong with the data retriving', async () => {
      findStub.rejects()
      await userController.getUsers(req, res, next)
      expect(findStub.calledOnce).to.be.true
      expect(next.calledOnce).to.be.true
    })

  })

  describe('# signUp()', () => {
    let saveStub
    let hashPasswordStub

    before(() => {
      saveStub = sinon.stub(User.prototype, 'save');
      hashPasswordStub = sinon.stub(passwordHash, 'generate')
    })

    afterEach(() => {
      findOneStub.reset()
      saveStub.reset()
      validationResultStub.reset()
    })

    it('should exit with error code if the registration data is not correct', async () => {
      validationResultStub.returns({ errors: [ { value: 'test', msg: 'Password should...', param: 'password', location: 'body' } ] })
      await userController.signUp(req, res, next)
      expect(next.calledOnce).to.be.true
      expect(validationResultStub.calledOnce).to.be.true
      expect(findOneStub.called).to.be.false
      expect(res.send.called).to.be.false
    })

    it('should interrupt the sign up process if the user already exists', async () => {
      validationResultStub.returns({ errors: [] })
      findOneStub.resolves({ name: 'testUser' })
      await userController.signUp(req, res, next)
      expect(next.calledOnce).to.be.true
      expect(validationResultStub.calledOnce).to.be.true
      expect(findOneStub.calledOnce).to.be.true
      expect(res.send.called).to.be.false
      expect(saveStub.called).to.be.false
    })

    it('should register new user', async () => {
      validationResultStub.returns({ errors: [] })
      findOneStub.resolves()
      mapperStub.returns({ name: 'testName' })
      req.body.name = 'John'
      req.body.email = 'john@gmail.com'
      req.body.password = 'testtest'
      hashPasswordStub.returns('hashed')
      await userController.signUp(req, res, next)
      expect(saveStub.called).to.be.true
      expect(saveStub.getCall(0).thisValue.email).equal('john@gmail.com')
      expect(res.send.called).to.be.true
    })
  })

  describe('# signIn()', () => {
    let jwtSignStub
    let pwdHashVerifyStub

    before(() => {
      pwdHashVerifyStub = sinon.stub(passwordHash, 'verify')
      jwtSignStub = sinon.stub(jsonWebToken, 'sign')
    })

    afterEach(() => {
      findOneStub.reset()
      jwtSignStub.reset()
      validationResultStub.reset()
      pwdHashVerifyStub.reset()
    })

    it('should generate JWT token if the credentials are correct', async () => {
      validationResultStub.returns({errors: []})
      findOneStub.resolves({email: 'test@test.com', role: 'User'})
      pwdHashVerifyStub.returns(true)
      await userController.signIn(req, res, next)
      expect(jwtSignStub.calledOnce).to.be.true
    })

    it('should throw an error if the credentials are not correct', async () => {
      validationResultStub.returns({errors: []})
      findOneStub.resolves({email: 'test@test.com', role: 'User'})
      pwdHashVerifyStub.returns(false)
      await userController.signIn(req, res, next)
      expect(findOneStub.calledOnce).to.be.true
      expect(next.calledOnce).to.be.true
    })
  })
})