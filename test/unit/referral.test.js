/* eslint-disable no-undef */
const { expect } = require('chai')
const sinon = require('sinon')

const referralController = require('../../src/controller/referral')
const User = require('../../src/model/user')
const Referral = require('../../src/model/referral')
const logger = require('../../src/helper/logger')

describe('# Referral link generation related logic', () => {
  let req
  let res
  let next
  let sandbox
  let userFindByIdStub
  let refFindOneStub
  let refSaveStub
  let refConstructorStub

  before(() => {
    req = {
      body: {}
    }
    res = {
      send: sinon.spy()
    }
    next = sinon.spy()
    userFindByIdStub = sinon.stub(User, 'findById')
    refFindOneStub = sinon.stub(Referral, 'findOne')
    refSaveStub = sinon.stub(Referral.prototype, 'save')
    refConstructorStub = sinon.stub(Referral.prototype, 'constructor')

    sandbox = sinon.createSandbox()
    sandbox.stub(logger, 'error')
    sandbox.stub(logger, 'info')
  })

  afterEach(() => {
    res.send.resetHistory()
    next.resetHistory()
    userFindByIdStub.reset()
    refFindOneStub.reset()
    refSaveStub.reset()
    refConstructorStub.reset()
  })

  after(() => {
    userFindByIdStub.restore()
    refFindOneStub.restore()
    refSaveStub.restore()
    sandbox.restore()
    refConstructorStub.restore()
  })

  describe('# getReferral()', () => {
    it('should return the referral link if the current user already has one', async () => {
      req.user = { id: 15 }
      userFindByIdStub.resolves({_id: 15})
      refFindOneStub.resolves({ link: 'apple' })
      await referralController.getReferral(req, res, next)
      expect(refSaveStub.called).to.be.false
      expect(res.send.getCall(0).lastArg.link).to.be.equal('apple')
    })

    it('should generate a new referral if the current user does not have one', async () => {
      req.user = { id: 15 }
      userFindByIdStub.resolves({ _id: 15, name: 'apple', email: 'apple@apple.com' })
      refFindOneStub.resolves()
      refSaveStub.resolves({ link: 'ref' })
      await referralController.getReferral(req, res, next)
      expect(refSaveStub.called).to.be.true
      expect(res.send.called).to.be.true
    })
  })

  describe('# confirmRefLinkRegistrationSteps()', () => {
    let user
    let referral
    let linkOwner

    before(() => {
      referral = {
        referred_users: {
          push: sinon.spy()
        },
        save: sinon.spy()
      }
      user = {
        save: sinon.spy(),
        wallet: 0
      }
      linkOwner = {
        save: sinon.spy(),
        wallet: 0
      }
    })

    afterEach(() => {
      referral.referred_users.push.resetHistory()
      referral.save.resetHistory()
      user.wallet = 0
      user.save.resetHistory()
      linkOwner.wallet = 0
      linkOwner.save.resetHistory()

    })

    it('should recognize that the link is not correct and leave the registration as a standard registration', async () => {
      refFindOneStub.resolves()
      await referralController.confirmRefLinkRegistrationSteps(null, null , 'testLink')
      expect(refFindOneStub.called).to.be.true
    })

    it('should register new user into the registered_userd list within the referral object', async () => {
      refFindOneStub.resolves(referral)
      userFindByIdStub.resolves(linkOwner)
      await referralController.confirmRefLinkRegistrationSteps(null, user, 'testLink')
      expect(referral.referred_users.push.called).to.be.true
    })

    it('should give $10 to the newly registered user', async () => {
      refFindOneStub.resolves(referral)
      userFindByIdStub.resolves(linkOwner)
      expect(user.wallet).to.be.equal(0)
      await referralController.confirmRefLinkRegistrationSteps(null, user, 'testLink')
      expect(user.wallet).to.be.equal(10)
      expect(user.save.called).to.be.true
    })

    it('should give $10 to the inviter user after every 5th new registered user', async () => {
      referral.referred_users.length = 5
      refFindOneStub.resolves(referral)
      userFindByIdStub.resolves(linkOwner)
      expect(linkOwner.wallet).to.be.equal(0)
      await referralController.confirmRefLinkRegistrationSteps(null, user, 'testLink')
      expect(linkOwner.wallet).to.be.equal(10)
      expect(linkOwner.save.called).to.be.true
    })
  })
})
