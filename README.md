# Amitree Refferal Program - AmReP

This project presents a solution regarding a customer referral program.

## Getting Started

These instructions will help you to get a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to be installed on your computer

```
node && npm
```

### Installing

Install dependencies

```
npm install
```

Start process

```
npm start
```

## Running the tests

Execute unit tests

```
npm run test-unit
```
Execute end to end tests. For this operation you need a running MongoDB instance on your computer. This example works with container MongoDB istance. If you prefer some other solution like a managed instance, in this case please update the MONGO_HOST env-variable in the /test/.env file.

```
docker run -d -p 27017:27017 --name amrep mongo
npm run test-int
```

Execute coding style test

```
npm run lint
```

## Deployment

Just commit and push you changes, the GitLab CI/CD take care.

## Authors

* **Robert-Sandor Balazsi**