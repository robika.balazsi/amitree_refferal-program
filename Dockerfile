FROM node:12-alpine3.9

WORKDIR /amitree-referral-program

# Copy source code
COPY package*.json ./

COPY src ./src

COPY public ./public

# Install production dependencies.
RUN npm install --only=production

ARG BUILD_TIME_ENV=dev

ENV NODE_ENV=$BUILD_TIME_ENV

ENV PORT=80

# Run the web service on container startup.
CMD [ "npm", "start" ]