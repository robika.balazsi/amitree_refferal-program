const expressJwt = require('express-jwt')
const httpStatus = require('http-status')
const { APIError } = require('../helper/error')
const config = require('../config/config')

// middleware for getting and validating the JSON Web Token, jwt-payload object will be part of the req.user
const jwtValidation = expressJwt({
	secret: config.secret,
	credentialsRequired: true,
	getToken: function fromHeader(req) {
		if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
			return req.headers.authorization.split(' ')[1]
		}

		return null
	}
})

/**
 * Provides a list of middleware that can manage the authorization process
 * @param {String|String[]} roles
 * @returns {Middleware[]} - List of middlewares
 */
const authorize = (roles = []) => {
	if (typeof roles === 'string') {
		roles = [roles]
	}

	return [
		jwtValidation,
		(req, res, next) => {
			if (roles.length && !roles.includes(req.user.role)) {
				next(new APIError('Unauthorized', httpStatus.UNAUTHORIZED))
			}
			// authentication and authorization successful
			next()
		}
	]
}

module.exports = authorize
