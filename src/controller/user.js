const { body } = require('express-validator')
const httpStatus = require('http-status')
const passwordHash = require('password-hash')
const jsonWebToken = require('jsonwebtoken')
const User = require('../model/user')
const DTOMappers = require('../model/data-tranfer-object/mappers')
const logger = require('../helper/logger')
const { APIError } = require('../helper/error')
const config = require('../config/config')
const validator = require('../helper/custom-express-validator')
const { confirmRefLinkRegistrationSteps } = require('./referral')

/**
 * Gets wallet amount.
 * @async
 * @controller
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Middleware function powered by Express
 */
const getWalletAmount = async (req, res, next) => {
  const userId = req.user.id
  try {
    const user = await User.findById(userId)
    res.send({ amount: user.wallet })
  } catch (err) {
    logger.error(err)
    next(new APIError(err.message))
  }
}

/**
 * Retrives all users.
 * @async
 * @controller
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Middleware function powered by Express
 */
const getUsers = async (req, res, next) => {
  try {
    const users = await User.find()
    res.send(users.map(DTOMappers.mapUserDTO))
  } catch (err) {
    logger.error(err)
    next(new APIError(err.message))
  }
}

/**
 * Gets account details of current user.
 * @async
 * @controller
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Middleware function powered by Express
 */
const getUser = async (req, res, next) => {
  const userId = req.user.id
  try {
    const user = await User.findById(userId)
    res.send(DTOMappers.mapUserDTO(user))
  } catch (err) {
    logger.error(err)
    next(new APIError(err.message))
  }
}

/**
 * Generates a new usser account.
 * @async
 * @controller
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Middleware function powered by Express
 */
const signUp = async (req, res, next) => {
  const [error] = validator.validationResult(req).errors
  if (error) {
    logger.info(`Invalid parameter error, '${error.param}' - ${error.msg}`)
    next(new APIError(`Invalid '${error.param}' parameter, ${error.msg}`, httpStatus.BAD_REQUEST))
    return
  }

  const { name, email, password } = req.body
  let user
  try {
    user = await User.findOne({ email })
    if (user) {
      logger.info(`User with ${email} already exists!`)
      return next(new APIError('User already exists', httpStatus.BAD_REQUEST))
    }

    // check if the user are using referral link during the registration
    const refer = req.get('refer')
    if (!refer) {
      // simple registration
      user = await new User({ name, email, password: passwordHash.generate(password) }).save()
    } else {
      // if there is refer header it means that the user has a referral link, in this case
      // based on the referral program rules the user gets extra money for the registration
      // Mongoose transaction helps to ensure the proper execution of the registration
      // Transaction is not supported on that DB side that I use (mLab free db)
      // because of this I just can mimic the transaction
      const session = null
      // const session = await mongoose.startSession()
      // session.startTransaction()
      user = await new User({ name, email, password: passwordHash.generate(password) }).save({ session })
      await confirmRefLinkRegistrationSteps(session, user, refer)
    }
    res.send(DTOMappers.mapUserDTO(user))
  } catch (err) {
    logger.error(err)
    next(err)
  }
}
/* Sign up data validation chain */
const signUpValidationChain = [
  body('name')
    .not().isEmpty()
    .isString(),
  body('email')
    .normalizeEmail()
    .isEmail().withMessage('Incorrect email address!'),
  body('password')
    .isLength({ min: 6, max: 25 }).withMessage('Password should contain 6-25 characters.')
    .matches(/\d/).withMessage('Password should contain at least one number.')
]


/**
 * Authenticates user.
 * @async
 * @controller
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Middleware function powered by Express
 */
const signIn = async (req, res, next) => {
  const [error] = validator.validationResult(req).errors
  if (error) {
    logger.info(`Invalid parameter error, '${error.param}' - ${error.msg}`)
    next(new APIError(`Invalid '${error.param}' parameter, ${error.msg}`, httpStatus.BAD_REQUEST))
    return
  }

  const { email, password } = req.body
  try {
    const user = await User.findOne({ email })
    if (user && passwordHash.verify(password, user.password)) {
      const userDTO = DTOMappers.mapUserDTO(user)
      const token = jsonWebToken.sign({ ...userDTO, role: user.role }, config.secret)

      return res.send({
        user: userDTO,
        token
      })
    }

    throw new APIError('Incorrect credential data!', httpStatus.BAD_REQUEST)
  } catch (err) {
    logger.error(err)
    next(err)
  }
}
/* Sign in data validation chain */
const signInValidationChain = [
  body('email')
    .normalizeEmail()
    .isEmail().withMessage('Incorrect email address!'),
  body('password').isString()
]

module.exports = {
  getUser,
  getUsers,
  getWalletAmount,
  signUpValidationChain,
  signUp,
  signInValidationChain,
  signIn
}
