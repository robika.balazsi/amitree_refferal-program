const mongoose = require('mongoose')
const User = require('../model/user')
const Referral = require('../model/referral')
const logger = require('../helper/logger')
const config = require('../config/config')
const { OperationalError } = require('../helper/error')

/**
 * Generates referral link.
 * @serviceMethod
 * @param {User} user
 * @returns {String}
 */
const generateReferralLink = (user) => {
  let personalChunk = user.name.toLowerCase().replace(/[^0-9a-z]/g, '')
  // check if the length of the personal part of the link is long enough
  if (personalChunk && personalChunk.length < 5) {
    personalChunk += user.email.split('@')[0].toLowerCase().replace(/[^0-9a-z]/g, '')
  }
  // Template of the link: PROTOCOL://DOMAIN/r/IDENTIFIER
  // IDENTIFIER has 2 parts: personal data chunk + random number
  return `http://${config.host}/r/${personalChunk.slice(0, 15)}${Math.floor(Math.random() * Math.floor(999))}`
}

/**
 * Manages and confirms the steps of the referral program logic, regardinf to the payments and operations
 * @serviceMethod
 * @param {Session} session - Helps to manage the MongoDB Transaction
 * @param {User} user - User object
 * @param {String} link - Referral link
 *
 * * Transaction related logic is commented out becase the mLab DB free version doesn't support this
 */
const confirmRefLinkRegistrationSteps = async (session, user, link) => {
  try {
    // Step 1: Check if the current link is valid
    const referral = await Referral.findOne({ link })
    if (!referral) {
      // the given referral link is not correct, the user will be signed up regulary
      // return session.commitTransaction()
      return
    }
    // Step 2: Adding the new user to the referral object
    referral.referred_users.push(user)
    await referral.save({ session })

    // Step 3: Adding $10 for registration with referral link
    user.wallet += 10
    await user.save({ session })

    // Step 4: Adding $10 for the inviter after every 5th new user
    if (referral.referred_users.length % 5 === 0) {
      const linkOwner = await User.findById(referral.user)
      linkOwner.wallet += 10
      await linkOwner.save({ session })
    }
    // return session.commitTransaction()
  } catch (err) {
    logger.error(err)
    // await session.abortTransaction()
    throw new OperationalError(err.message)
  }
}

/**
 * Gets referral link of the current user. Generartes a new one if the link doesn't exist yet.
 * @async
 * @controller
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Middleware function powered by Express
 */
const getReferral = async (req, res, next) => {
  const userId = req.user.id
  try {
    const user = await User.findById(userId)

    // find the user related referral link, create a new one if it does not exist
    let referral = await Referral.findOne({ user: mongoose.Types.ObjectId(user._id) })
    if (!referral) {
      referral = await new Referral({
        user,
        link: generateReferralLink(user)
      }).save()
    }
    res.send({ link: referral.link })
  } catch (err) {
    logger.error(err)
    next(err)
  }
}

module.exports = {
  getReferral,
  confirmRefLinkRegistrationSteps
}
