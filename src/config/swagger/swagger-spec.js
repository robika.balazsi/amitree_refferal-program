const path = require('path')
const swaggerJSDoc = require('swagger-jsdoc')

const swaggerDefinition = {
  swaggerDefinition: {
    openapi: '3.0.1',
    info: {
      title: 'Amitree Referral Program - AmReP',
      version: '1.0.0',
      description: 'RESTful API documentation for customer referral system.',
      contact: {
        name: 'Robert Sandor Balazsi',
        email: 'robika.balazsi42@gmail.com'
      }
    },
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        }
      }
    },
    security: [{
      bearerAuth: []
    }]
  },
  apis: [path.join(__dirname, '../../route/*.js')]
}

module.exports = swaggerJSDoc(swaggerDefinition)
