/* eslint-disable no-undef */
const path = require('path')

const env = process.env.NODE_ENV || 'development'
module.exports = {
  secret: 'uHFcySERUdjBMvfhbUvV6lGiu4rlIh7K',
  host: process.env.HOST || 'amitree.com',
  port: process.env.PORT || 3000,
  logging: {
    level: 'debug',
    dirname: path.join(__dirname, '/../../../amitree-ref-prog-log'),
    loggingToConsole: env === 'development'
  },
  mongoDB: {
    uri: process.env.MONGO_URI || 'mongodb://api-user:refprog2020@ds351827.mlab.com:51827/amitree-referral-program'
  }
}
