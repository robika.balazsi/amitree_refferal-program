const httpStatus = require('http-status')

/**
 * @extends Error
 */
class ExtendableError extends Error {
  constructor(message, isReportable) {
    super(message)
    this.name = this.constructor.name
    this.isReportable = isReportable // This flag shows that the error is notifiable or not for the developers/maintainers.
    this.isOperational = true
    Error.captureStackTrace(this, this.constructor.name)
  }
}

/**
 * Class representing an API error.
 * @extends ExtendableError
 */
class APIError extends ExtendableError {
  /**
   * Creates an API error.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {boolean} isPublic - Whether the message should be visible to user or not.
   * @param {boolean} isReportable - Defines that the error should be reported to developers/maintainters or not.
   */
  constructor(message, status = httpStatus.INTERNAL_SERVER_ERROR, isPublic = false, isReportable = false) {
    super(message, isReportable)
    this.status = status
    this.isPublic = isPublic
  }
}

/**
 * Class representing an Operational error.
 * @extends OperationalError
 */
class OperationalError extends ExtendableError {
  /**
   * Creates an Operational error.
   * @param {string} message - Error message.
   * @param {boolean} isReportable - Defines that the error should be reported to developers/maintainters or not.
   */
  constructor(message, isReportable = true) {
    super(message, isReportable)
  }
}

module.exports = {
  APIError,
  OperationalError
}
