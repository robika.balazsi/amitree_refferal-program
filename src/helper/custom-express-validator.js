const { validationResult } = require('express-validator')

const customValidationResult = validationResult.withDefaults({
  formatter: (error) => {
    return {
      myLocation: error.location,
    }
  }
})

module.exports = {
  validationResult: customValidationResult
}
