const winston = require('winston')
const WinstonDailyRotateFile = require('winston-daily-rotate-file')
const config = require('../config/config')

const logger = winston.createLogger({
  level: config.logging.level,
  format: winston.format.combine(
    winston.format.errors({ stack: true }),
    winston.format.json(),
    winston.format.timestamp(),
    winston.format.prettyPrint()
  ),
  transports: [
    new WinstonDailyRotateFile({
      filename: 'error.log',
      dirname: config.logging.dirname,
      level: 'error',
      datePattern: 'YYYY-MM-DD',
      maxSize: 20,
      maxFiles: 15,
      handleExceptions: true,
      timestamp: true
    }),
    new WinstonDailyRotateFile({
      filename: 'combined.log',
      dirname: config.logging.dirname,
      level: 'info',
      datePattern: 'YYYY-MM-DD',
      maxSize: 500,
      maxFiles: 5,
      timestamp: true,
      handleExceptions: true
    })
  ]
})

if (config.logging.loggingToConsole) {
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
    handleExceptions: true,
    timestamp: true
  }))
}

module.exports = logger
