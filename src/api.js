const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const swaggerUI = require('swagger-ui-express')
const swaggerSpec = require('./config/swagger/swagger-spec')
const logger = require('./helper/logger')
const config = require('./config/config')
const router = require('./route')

// connecting to database
mongoose.connect(config.mongoDB.uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
})
mongoose.connection.on('error', err => {
  logger.error(err)
  process.exit(1)
})

// configuring the RESTful API
const app = express()
app.use(bodyParser.json())
app.use('/api', router)

// configuring swagger
app.use(express.static('public'))
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpec))
app.use((err, req, res, next) => {
  // TODO: different logging strategy for different env
  const { status, message, stack, name } = err
  res.status(status || 500).json({
    name,
    status,
    message,
    stack
  })
})

// start app when the DB connection is ready
mongoose.connection.on('connected', () => {
  logger.info(`Mongoose connected to host: ${config.mongoDB.uri}`)
  app.listen(config.port, () => logger.info(`Amitree-Referral-Program RESTful API listening on port ${config.port}!`))
})

module.exports = app
