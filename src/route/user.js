const express = require('express')
const authorize = require('../controller/authorization')
const userController = require('../controller/user')
const Role = require('../helper/role')
const router = express.Router()

/**
 * @swagger
 *
 * components:
 *   schemas:
 *     UserCredential:
 *       type: object
 *       required:
 *         - email
 *         - password
 *       properties:
 *         email:
 *           type: string
 *         password:
 *           type: string
 *           format: password
 *     UserRegistration:
 *       allOf:
 *         - $ref: '#/components/schemas/UserCredential'
 *         - type: object
 *           required:
 *             - name
 *           properties:
 *             name:
 *               type: string
 *     User:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         name:
 *           type: string
 *         email:
 *           type: string
 */

/**
 * @swagger
 * /api/users/all:
 *   get:
 *     summary: Gets all available users. ADMIN
 *     tags:
 *       - user
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: List of registered users
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 */
router.get('/all',
  authorize(Role.Admin),
  userController.getUsers
)

/**
 * @swagger
 * /api/users:
 *   get:
 *     summary: Gets details of current user account.
 *     tags:
 *       - user
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Account details of current user
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 */
router.get('/',
  authorize([Role.User, Role.Admin]),
  userController.getUser
)

/**
 * @swagger
 * /api/users:
 *   post:
 *     summary: Sign up a new user.
 *     tags:
 *       - user
 *     security: []
 *     parameters:
 *      - in: header
 *        name: refer
 *        schema:
 *          type: string
 *        required: false
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UserRegistration'
 *     responses:
 *       200:
 *         description: Data of newly registered users.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 */
router.post('/',
  userController.signUpValidationChain,
  userController.signUp
)

/**
 * @swagger
 * /api/users/authenticate:
 *   post:
 *     summary: Authenticate the given user.
 *     tags:
 *       - user
 *     security: []
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UserCredential'
 *     responses:
 *       200:
 *         description: Data of authenticated user.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 */
router.post('/authenticate',
  userController.signInValidationChain,
  userController.signIn
)

/**
 * @swagger
 * /api/users/wallets:
 *   get:
 *     summary: Gets actual user wallet status.
 *     tags:
 *       - user
 *       - wallet
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Amount of wallet
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 amount:
 *                   type: number
 */
router.get('/wallets',
  authorize(Role.User),
  userController.getWalletAmount
)

module.exports = router
