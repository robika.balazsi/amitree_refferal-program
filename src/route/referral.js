const express = require('express')
const authorize = require('../controller/authorization')
const Role = require('../helper/role')
const referralController = require('../controller/referral')
const router = express.Router()

/**
 * @swagger
 * /api/referrals:
 *   get:
 *     summary: Gets referral link of the current user.
 *     tags:
 *       - referral
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Referral link.
 *         content:
 *           application/json:
 *             schema:
 *               name: Link
 *               type: String
 */
router.get('/',
  authorize(Role.User),
  referralController.getReferral
)

module.exports = router
