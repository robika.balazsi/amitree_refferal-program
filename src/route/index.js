const express = require('express')
const userRoute = require('./user')
const referralRoute = require('./referral')
const router = express.Router()

// define API routes
router.use('/users', userRoute)
router.use('/referrals', referralRoute)

module.exports = router
