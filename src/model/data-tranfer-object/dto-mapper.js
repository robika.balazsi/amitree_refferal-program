/**
 * Builds a list where only just the publicly available properties are there.
 * The private property of the schema definition determines public access
 * @param {Object} properties - Schema definition, where the keys are the name of the prop and the value is their configs. ({name: {private: true, required:true}})
 * @returns {Array} - Array with publicly available properties' name
 */
function publicPropertyBuilder(properties) {
  return Object.getOwnPropertyNames(properties)
    .filter(name => properties[name] && !properties[name].private)
}

/**
 * Maps business entity to data transfer object
 * @param {Model} entity - Mongoose model object
 */
const mapDTO = (entity, publicProperties) => {
  const entityDTO = {}

  entityDTO.id = entity._id
  publicProperties.forEach(item => {
    entityDTO[item] = entity[item]
  })

  return entityDTO
}

module.exports = {
  publicPropertyBuilder,
  mapDTO
}
