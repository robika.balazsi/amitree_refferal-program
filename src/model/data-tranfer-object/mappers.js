const DTOMapper = require('./dto-mapper')
const User = require('../user')
const Referral = require('../referral')

// Define UserDTO using the dto-mapper module
const publicUserProperties = DTOMapper.publicPropertyBuilder(User.schema.obj)
const mapUserDTO = (user) => DTOMapper.mapDTO(user, publicUserProperties)

// Define ReferralDTO using the dto-mapper module
const publicReferralProperties = DTOMapper.publicPropertyBuilder(Referral.schema.obj)
const mapReferralDTO = (referral) => DTOMapper.mapDTO(referral, publicReferralProperties)

module.exports = {
  mapUserDTO,
  mapReferralDTO
}
