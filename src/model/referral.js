const mongoose = require('mongoose')
const Schema  = mongoose.Schema

const ReferralSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    unique: true,
    required: true
  },
  link: {
    type: String,
    unique: true,
    required: true
  },
	referred_users: [{
    type: Schema.ObjectId,
    ref: 'User',
    unique: false,
    required: true,
    private: true
  }]
})

module.exports = mongoose.model('Referral', ReferralSchema)
