const mongoose = require('mongoose')
const Role = require('../helper/role')
const Schema  = mongoose.Schema

const userSchema = new Schema({
  name: {
    type: String,
    unique: false,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
	password: {
    type: String,
    unique: false,
    required: true,
    private: true
  },
  wallet: {
    type: Number,
    default: 0,
    unique: false,
    required: true,
    private: true
  },
  role: {
    type: String,
    default: Role.User,
    required: true,
    private: true
  }
})

module.exports = mongoose.model('User', userSchema)
